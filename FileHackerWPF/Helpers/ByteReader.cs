﻿namespace fileHacker
{
    using System.IO;

    public class ByteReader
    {
        public static byte[] ReadDataFromFile(string fileName)
        {
            using (var inputFileStream = new FileStream(fileName, FileMode.Open))
            using (var myBinaryReader = new BinaryReader(inputFileStream))
            {
                return myBinaryReader.ReadBytes((int)inputFileStream.Length);

            }
        }
    }
}
