﻿using System;
using System.Drawing;

namespace FileHackerWPF.Helpers
{
    class DataArrayImageDrawer
    {
        public Bitmap GetBitmap(byte[] dataArray)
        {
            int graphWidth = (int)Math.Floor(Math.Sqrt(dataArray.Length));
            int graphHeight = (int)Math.Floor(dataArray.Length / (double)graphWidth);

            var image = new Bitmap(graphWidth, graphHeight);
            using (var graphics = Graphics.FromImage(image))
            {
                for (int i = 0; i < dataArray.Length; i++)
                {
                    using (var brush = new SolidBrush(Color.FromArgb(dataArray[i], dataArray[i], dataArray[i])))
                    {
                        graphics.FillRectangle(
                            brush,
                            i % graphWidth,
                            i / graphWidth,
                            1,
                            1);
                    }
                }
            }
            return image;
        }
    }
}
