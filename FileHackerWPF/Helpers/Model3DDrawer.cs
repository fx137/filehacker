﻿using FileHackerWPF.Models;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Core;
using SharpDX;
using System;

namespace FileHackerWPF.Helpers
{
    internal class Model3DDrawer
    {
        private PointGeometry3D points;

        private Color4Collection colors;
        private Vector3Collection positions;
        private IntCollection indices;

        private IData3D data3d;

        private int index;

        public PointGeometry3D GetModelFrom3DArray(IData3D data3d)
        {
            this.data3d = data3d;
            points = new PointGeometry3D();
            colors = new Color4Collection();
            positions = new Vector3Collection();
            indices = new IntCollection();

            index = 0;

            AddPoints();

            points.Positions = positions;
            points.Colors = colors;
            points.Indices = indices;

            points.UpdateOctree();
            points.UpdateTriangles();
            points.UpdateVertices();

            return points;
        }

        private void AddPoints()
        {
            for (int x = 0; x < data3d.XSize; x++)
            {
                for (int y = 0; y < data3d.YSize; y++)
                {
                    for (int z = 0; z < data3d.ZSize; z++)
                    {
                        AddPoint(x, y, z);
                    }
                }
            }
        }

        void AddPoint(int x, int y, int z)
        {
            float relX, relY, relZ;
            double size = 1 / (double)data3d.XSize / 2.0;
            double halfSize = size / 2.0;

            relX = x / (float)data3d.XSize - 0.5f;
            relY = y / (float)data3d.YSize - 0.5f;
            relZ = z / (float)data3d.ZSize - 0.5f;

            double relStrenght = 0;
            if (data3d.MaxCount > 0) { relStrenght = data3d.Counts[x, y, z] / (double)data3d.MaxCount; }

            indices.Add(index++);
            positions.Add(new Vector3(relX, relY, relZ));

            double intensity = relStrenght * .9 + .1;
            colors.Add(new Color4(1, 1, 1, (float)(intensity)));
        }
    }
}
