﻿using Caliburn.Micro;
using FileHackerWPF.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;

namespace FileHackerWPF
{
    class MainBootStrapper : BootstrapperBase
    {
        private readonly IKernel kernel = new StandardKernel();

        public MainBootStrapper()
        {
            kernel.Bind<IDataArrayViewModel>().To<DataArrayViewModel>();
            kernel.Bind<IDetailsViewModel>().To<DetailsViewModel>();
            kernel.Bind<IHistogramViewModel>().To<HistogramViewModel>();
            kernel.Bind<IMainViewModel>().To<MainViewModel>();
            kernel.Bind<ISpaceViewModel>().To<SpaceViewModel>();

            kernel.Bind<IWindowManager>().To<WindowManager>();

            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IMainViewModel>();
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            MessageBox.Show(e.Exception.Message, "An error as occurred", MessageBoxButton.OK);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            return kernel.Get(serviceType, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}
