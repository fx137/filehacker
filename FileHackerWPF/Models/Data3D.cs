﻿using System;

namespace FileHackerWPF.Models
{
    public interface IData3D
    {
        int[,,] Counts { get; }
        int MaxCount { get; }
        int XSize { get; }
        int YSize { get; }
        int ZSize { get; }

        void CalculateCounts();
    }

    public class Data3D : IData3D
    {
        private readonly byte[] fileData;
        private readonly int decimator;

        public int MaxCount { get; private set; } = 0;

        public int XSize => Counts.GetLength(0);
        public int YSize => Counts.GetLength(1);
        public int ZSize => Counts.GetLength(2);

        public int[,,] Counts { get; }

        public Data3D(byte[] fileData, int decimator = 32)
        {
            if ((256.0 / decimator) % 1 != 0)
            {
                throw new ArgumentException("Invalid Decimator, has follow the rule: 256.0 / decimator % 1 == 0 ");
            }

            this.fileData = fileData;
            this.decimator = decimator;
            Counts = new int[256 / decimator, 256 / decimator, 256 / decimator];
        }

        public void CalculateCounts()
        {
            for (int i = 0; i < fileData.Length - 2; i++)
            {
                Counts[
                    fileData[i] / decimator,
                    fileData[i + 1] / decimator,
                    fileData[i + 2] / decimator
                    ]++;
            }
            CalculateMaxCount();
        }

        private void CalculateMaxCount()
        {
            MaxCount = 0;

            for (int x = 0; x < XSize; x++)
            {
                for (int y = 0; y < YSize; y++)
                {
                    for (int z = 0; z < ZSize; z++)
                    {
                        MaxCount = Math.Max(MaxCount, Counts[x, y, z]);
                    }
                }
            }
        }
    }
}
