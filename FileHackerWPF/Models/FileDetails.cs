﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FileHackerWPF.Models
{
    public interface IFileDetails
    {
        double Entropy { get; }
        string MagicNumber { get; }
    }

    public class FileDetails : IFileDetails
    {
        private readonly IHistogram histogram;
        private readonly IEnumerable<byte> fileData;
        private double entropy = 0;

        public FileDetails(IHistogram histogram, IEnumerable<byte> fileData)
        {
            this.histogram = histogram;
            this.fileData = fileData;
            CalculateEntropy();
        }

        public double Entropy => -entropy;
        public string MagicNumber { get; private set; }

        private void CalculateEntropy()
        {
            var counts = histogram.Counts;
            double relativeCount = 0;

            for (int i = 0; i < 255; i++)
            {
                if (counts[i] > 0)
                {
                    relativeCount = counts[i] / (double)counts.Values.Sum();
                    entropy += relativeCount * Math.Log(relativeCount, 2);
                }
            }
        }
    }
}
