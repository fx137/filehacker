﻿using System.Collections.Generic;
using System.Linq;

namespace FileHackerWPF.Models
{
    public interface IHistogram
    {
        IDictionary<int, int> Counts { get; }

        void AddDataPoint(byte dataPoint);
    }

    public sealed class Histogram : IHistogram
    {
        public IDictionary<int, int> Counts { get; } = new Dictionary<int, int>();

        public Histogram(IEnumerable<byte> data)
        {
            FillDictionary();
            AddPoints(data);
        }

        private void FillDictionary()
        {
            foreach (int a in Enumerable.Range(0, 255))
            {
                Counts.Add(a, 0);
            }
        }

        private void AddPoints(IEnumerable<byte> data)
        {
            foreach (byte readByte in data)
            {
                AddDataPoint(readByte);
            }
        }

        public void AddDataPoint(byte dataPoint)
        {
            if (Counts.ContainsKey(dataPoint))
            {
                Counts[dataPoint]++;
            }
            else
            {
                Counts.Add(dataPoint, 1);
            }
        }
    }
}

