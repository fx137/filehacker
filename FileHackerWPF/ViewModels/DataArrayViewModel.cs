﻿using Caliburn.Micro;
using FileHackerWPF.Helpers;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace FileHackerWPF.ViewModels
{
    public interface IDataArrayViewModel : IInitializeable
    {
        BitmapImage DataArrayImage { get; }
    }

    public class DataArrayViewModel : Screen, IDataArrayViewModel
    {
        public DataArrayViewModel(Image dataArrayImage)
        {
            this.dataArrayImage = dataArrayImage;
        }
        
        private readonly Image dataArrayImage;

        public BitmapImage DataArrayImage { get; private set; }

        public void Initialize()
        {
            DataArrayImage = Utils.BitmapToImageSource(dataArrayImage ?? Utils.GenerateWhiteImage());
        }
    }
}
