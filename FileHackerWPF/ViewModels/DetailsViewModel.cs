﻿using Caliburn.Micro;
using FileHackerWPF.Models;
using LiveCharts;

namespace FileHackerWPF.ViewModels
{
    public interface IDetailsViewModel : IInitializeable
    {
        IChartValues Entropy { get; }
        IChartValues Rest { get; }
    }

    public class DetailsViewModel : Screen, IDetailsViewModel
    {
        public IChartValues Entropy { get; private set; } 
        public IChartValues Rest { get; private set; }

        private readonly IFileDetails fileDetails;

        public DetailsViewModel(IFileDetails fileDetails)
        {
            this.fileDetails = fileDetails;
        }

        public void Initialize()
        {
            Entropy = new ChartValues<double>() { fileDetails.Entropy };
            Rest = new ChartValues<double>() { 8.0 - fileDetails.Entropy };
        }
    }
}
