﻿using Caliburn.Micro;
using FileHackerWPF.Models;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Linq;

namespace FileHackerWPF.ViewModels
{
    public interface IHistogramViewModel : IInitializeable
    {
        SeriesCollection SeriesCollection { get; set; }
    }

    public class HistogramViewModel : Screen, IHistogramViewModel
    {
        private readonly IHistogram histogram;

        public HistogramViewModel(IHistogram histogram)
        {
            this.histogram = histogram;
        }

        public SeriesCollection SeriesCollection { get; set; }

        public void Initialize()
        {
            SeriesCollection = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = null,
                    LabelPoint = LabelPointCallback,
                    Values = new ChartValues<double>(histogram.Counts.Values.Select(d=>(double)d))
                }
            };
        }

        private string LabelPointCallback(ChartPoint arg)
        {
            return $"Value: 0x{((int)arg.X):X2} {Environment.NewLine}Count: {arg.Y}";
        }
    }
}
