﻿using Microsoft.Win32;
using System.IO;
using Caliburn.Micro;
using FileHackerWPF.Models;
using System.Threading.Tasks;
using FileHackerWPF.Helpers;
using System.Drawing;
using Ninject;

namespace FileHackerWPF.ViewModels
{
    internal interface IMainViewModel
    {
        IDataArrayViewModel DataArray { get; }
        IDetailsViewModel Details { get; }
        IHistogramViewModel Histogram { get; }
        ISpaceViewModel SpaceView { get; }

        string FileName { get; }
        bool Loading { get; set; }
        int ZHeight { get; }

        Task LoadFile(object obj);
    }

    internal sealed class MainViewModel : Screen, IMainViewModel
    {
        private bool loading = false;
        private readonly IKernel kernel;

        public string FileName { get; private set; }

        public bool Loading
        {
            get
            {
                return loading;
            }
            set
            {
                loading = value;
                Refresh();
            }
        }

        public int ZHeight => loading ? 20 : 0;

        public IDataArrayViewModel DataArray { get; private set; }
        public IHistogramViewModel Histogram { get; private set; }
        public ISpaceViewModel SpaceView { get; private set; }
        public IDetailsViewModel Details { get; private set; }

        public MainViewModel(IKernel kernel)
        {
            this.kernel = kernel;
        }


        public async Task LoadFile(object obj)
        {
            var odf = new OpenFileDialog();

            if (odf.ShowDialog() ?? false)
            {
                FileName = odf.FileName;

                byte[] fileData = File.ReadAllBytes(FileName);


                Loading = true;

                IHistogram histogram = null;
                IFileDetails fileDetails = null;
                IData3D data3D = null;
                Bitmap dataArrayImage = null;

                await Task.Run(() => ProcessData(fileData, out histogram, out fileDetails, out data3D, out dataArrayImage));

                DataArray = new DataArrayViewModel(dataArrayImage);
                Histogram = new HistogramViewModel(histogram);
                SpaceView = new SpaceViewModel(data3D);
                Details = new DetailsViewModel(fileDetails);

                DataArray.Initialize();
                Histogram.Initialize();
                SpaceView.Initialize();
                Details.Initialize();

                Loading = false;

                Refresh();
            }
        }

        private void ProcessData(byte[] fileData, out IHistogram histogram, out IFileDetails fileDetails, out IData3D data3D, out Bitmap dataArrayImage)
        {
            histogram = new Histogram(fileData);
            fileDetails = new FileDetails(histogram, fileData);
            data3D = new Data3D(fileData, 32);
            data3D.CalculateCounts();
            var imageDrawer = new DataArrayImageDrawer();
            dataArrayImage = imageDrawer.GetBitmap(fileData);
        }
    }
}
