﻿using Caliburn.Micro;
using FileHackerWPF.Helpers;
using FileHackerWPF.Models;
using HelixToolkit.Wpf.SharpDX;
using Media3D = System.Windows.Media.Media3D;

namespace FileHackerWPF.ViewModels
{
    public interface ISpaceViewModel : IInitializeable
    {
        Camera Camera { get; }
        EffectsManager EffectsManager { get; }
        PointGeometry3D PointGeometry { get; }
        System.Windows.Size PointSize { get; }
    }

    public class SpaceViewModel : Screen, ISpaceViewModel
    {
        private readonly IData3D data3D;

        public SpaceViewModel(IData3D data3D)
        {
            this.data3D = data3D;
        }

        public Camera Camera { get; private set; }

        public PointGeometry3D PointGeometry { get; private set; }

        public EffectsManager EffectsManager { get; } = new DefaultEffectsManager();

        public System.Windows.Size PointSize => new System.Windows.Size(10, 10);

        public void Initialize()
        {
            SetupCamera();
            GenerateData3D();
            Refresh();
        }

        private void GenerateData3D()
        {
            var modelDrawer = new Model3DDrawer();
            PointGeometry = modelDrawer.GetModelFrom3DArray(data3D);
        }

        private void SetupCamera()
        {
            Camera = new OrthographicCamera()
            {
                Position = new Media3D.Point3D(-.5, -1, 1),
                LookDirection = new Media3D.Vector3D(.5, 1, -1),
                NearPlaneDistance = 0.1,
                FarPlaneDistance = 5,
                Width = 2.5,
            };
        }
    }
}
